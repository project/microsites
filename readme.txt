
MICROSITES
=======================

Microsites provides a site-within-a-site capability, using a plugin-based
architecture.

Microsites are individual nodes which are specially enabled to provide their 
own context, menu, breadcrumb and other settings.

The Microsites framework does all the heavy lifting, data storage and configuration
setting for modules which implement its API. At its most basic, Microsites
leveraged Context and the Drupal Menu system to create a virtual site-within-a-site.

Modules can implement Microsites functionality by providing ctools plugin which
extends the BasicMicrositesExtension class. See existing plugins for examples.

Plugin behaviour can also be triggered from anywhere in your code using the
function microsites_trigger_callback(), which is documented in microsites.module.

Required modules:
  Context
  Custom Menu
  Menu Block
  Xtfers Utilities
  
Recommended Modules
  Menu Breadcrumb